package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 *
 */
public class View extends Application {
    final static DragLabel dragLabel = new DragLabel();

    public static void main(String[] args) {
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {



        Parent root = fxmlSceneGraph();


        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Teach Me Words");

        primaryStage.show();

    }

    private Parent fxmlSceneGraph() throws IOException {
        return FXMLLoader.load(getClass().getResource("/TeachMeWords.fxml"));
    }
}
