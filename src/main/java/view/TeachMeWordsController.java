package view;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import java.util.List;
import java.util.Random;

import static view.View.dragLabel;


/**
 *
 */

public class TeachMeWordsController {

    @FXML
    private Button exitButton;

    @FXML
    private Button loadButton;

    @FXML
    private Label welcomeLabel;
    @FXML
    private Label label;

    @FXML
    private AnchorPane anchorPane;

    private int i = 40;

    private int randomPosition() {

        Random random = new Random();

        int randomPoint = random.nextInt(400);
        return randomPoint;
    }


    @FXML
    void exitButtonPressed(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void loadButtonPressed(ActionEvent event) {

        List<String> listOfWordsFromRestService = RestLoader.getWordsFromRestService();
        if ((listOfWordsFromRestService == null) || (listOfWordsFromRestService.isEmpty())) {
            Label errorLabel = new Label("Huston we have a problem...");
            errorLabel.setLayoutX(randomPosition() + 50);
            errorLabel.setLayoutY(randomPosition() + 50);

            dragLabel.moveLabel(errorLabel);
            anchorPane.getChildren().addAll(errorLabel);
        } else {
            for (String word : listOfWordsFromRestService) {
                Label newLabel = new Label(word);
                newLabel.setStyle("-fx-border-color: green; -fx-border-width: 4;");
                newLabel.setLayoutX(randomPosition() + 50);
                newLabel.setLayoutY(randomPosition() + 50);

                dragLabel.moveLabel(newLabel);
                anchorPane.getChildren().addAll(newLabel);
            }


        }

    }


}

