package view;


import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;

/**
 *
 */
public class DragLabel {
    private double mousePressedPosX;
    private double mousePressedPosY;
    private double initialLabelTranslateX;
    private double initialLabelTranslateY;


    public Node moveLabel(Node node) {

        node.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mousePressedEvent) {
                mousePressedPosX = mousePressedEvent.getSceneX();
                mousePressedPosY = mousePressedEvent.getSceneY();
                initialLabelTranslateX = node.getTranslateX();
                initialLabelTranslateY = node.getTranslateY();
                node.setCursor(Cursor.HAND);
            }
        });

        node.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseDraggedEvent) {
                node.setTranslateX(mouseDraggedEvent.getSceneX() + initialLabelTranslateX - mousePressedPosX);
                node.setTranslateY(mouseDraggedEvent.getSceneY() + initialLabelTranslateY - mousePressedPosY);
            }
        });

        node.setOnMouseReleased(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                node.setCursor(Cursor.DEFAULT);
            }
        });

        return node;
    }

}
