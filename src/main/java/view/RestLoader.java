package view;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 *
 */
public class RestLoader {

    public static List<String> getWordsFromRestService() {

        List<String> outputList = Collections.emptyList();

        try {
            Client client = Client.create();
            WebResource target = client.resource("http://localhost:8080/teach-net-1.0-SNAPSHOT/matchwords/");
            ClientResponse response = target.accept("text/plain").get(ClientResponse.class);
            System.out.println(response.getStatus());
            if (response.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
            }
            outputList = Arrays.asList(response.getEntity(String.class).split("\\s* \\s*"));
        } catch (Exception e) {
            System.out.println("Server: Something goes wrong...");
        }

        return outputList;
    }

}
