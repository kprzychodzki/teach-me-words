# TEACH ME WORDS #

### Basic information ###
The idea of this app is to allow remote communication between 'teacher' and 'student'  - in real time. Teach Me Words - is a GUI create with JavaFx which is communicating with servlet TeachNet REST Service, using REST. 
Client requirement is to have a substitute of table - so, for example matching words must be a manual process of dragging elements - like in real world.

### Description ###
As a target application will be able to run two tasks: match words and fill-the-gap quiz.

* Match words - from REST Service user will obtain few words that will be changed to draggable labels. Student's task is to pair these labels manualy (client requirement) and teacher sees what is happening in real-time. 
* Quiz - questiones will be stored at MySQL database, student will write answer into text field and send this to database.